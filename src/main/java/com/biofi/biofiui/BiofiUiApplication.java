package com.biofi.biofiui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
//@EnableOAuth2Client
@SpringBootApplication
public class BiofiUiApplication {

  public static void main(String[] args) {
    SpringApplication.run(BiofiUiApplication.class, args);
  }

}
