import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { UserFormComponent } from './admin/user-form/user-form.component';
import { ChangePasswordComponent } from './admin/change-password/change-password.component';
import { DeleteUserComponent } from './admin/delete-user/delete-user.component';
import { UserTableComponent } from './admin/user-table/user-table.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'admin',
    loadChildren: 'app/admin/admin.module#AdminModule'
  },
  // {
  //   path: 'add-user',
  //   component: UserFormComponent
  // },
  // {
  //   path: 'change-password',
  //   component: ChangePasswordComponent
  // },
  // {
  //   path: 'delete-user',
  //   component: DeleteUserComponent
  // },
  // {
  //   path: 'get-users',
  //   component: UserTableComponent
  // },
  // {
  //   path: 'enroll',
  //   component: UserFormComponent
  // },
  // {
  //   path: 'match',
  //   component: UserFormComponent
  // }
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  // },
  // {
  //   path: '**',
  //   component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
