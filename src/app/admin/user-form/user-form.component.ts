import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../core/api.service';

@Component({
  selector: 'bf-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  userForm: FormGroup;
  institutes;

  constructor(private formBuilder: FormBuilder, private api: ApiService) {}

  ngOnInit() {
    this.api.getInstitutes().subscribe((institutes) => {
      this.institutes = institutes;
    });

    this.userForm = this.formBuilder.group({
      userName: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required],
      'c-password': ['', Validators.required],
      institute: ['', Validators.required],
      role: ['', Validators.required]
    });
  }

  onSubmit() {
    this.api.addUser(this.userForm.value).subscribe((success) => console.log('s', success));
  }
}
