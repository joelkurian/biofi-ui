import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { ApiService } from '../../core/api.service';
import { ConfirmDeleteComponent } from '../confirm-delete/confirm-delete.component';
import { ChangePasswordModalComponent } from '../change-password-modal/change-password-modal.component';

@Component({
  selector: 'bf-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit {
  displayedColumns = ['id', 'name', 'role', 'actions'];
  dataSource;
  constructor(private api: ApiService, public dialog: MatDialog) {}

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.api.getUsers().subscribe((users) => {
      this.dataSource = new MatTableDataSource(users);
    });
  }

  deleteUser(user) {
    const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
      width: '250px',
      data: { objName: user.userName, obj: user }
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.api.deleteUser(result.id).subscribe(() => this.getUsers());
    });
  }

  changePassword(user) {
    console.log(user);

    const dialogRef = this.dialog.open(ChangePasswordModalComponent, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
      console.log('r', result);
      // this.animal = result;
      this.api.changePasswordByAdmin(user.id, result).subscribe();
    });
  }
}
