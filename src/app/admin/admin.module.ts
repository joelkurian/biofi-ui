import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { UserFormComponent } from './user-form/user-form.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { DeleteUserComponent } from './delete-user/delete-user.component';
import { UserTableComponent } from './user-table/user-table.component';
import { SharedModule } from '../shared/shared.module';
import { AdminRoutingModule } from './admin-routing.module';
import { ConfirmDeleteComponent } from './confirm-delete/confirm-delete.component';
import { InstituteTableComponent } from './institute-table/institute-table.component';
import { InstituteFormComponent } from './institute-form/institute-form.component';
import { ChangePasswordModalComponent } from './change-password-modal/change-password-modal.component';

@NgModule({
  imports: [SharedModule, AdminRoutingModule],
  declarations: [
    AdminComponent,
    UserFormComponent,
    ChangePasswordComponent,
    DeleteUserComponent,
    UserTableComponent,
    ConfirmDeleteComponent,
    InstituteTableComponent,
    InstituteFormComponent,
    ChangePasswordModalComponent
  ],
  entryComponents: [ConfirmDeleteComponent, ChangePasswordModalComponent]
})
export class AdminModule {}
