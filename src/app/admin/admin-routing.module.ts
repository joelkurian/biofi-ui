import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  UserFormComponent,
  ChangePasswordComponent,
  DeleteUserComponent,
  UserTableComponent,
  InstituteTableComponent,
  InstituteFormComponent
} from '.';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'add-user',
        component: UserFormComponent
      },
      {
        path: 'change-password',
        component: ChangePasswordComponent
      },
      {
        path: 'delete-user',
        component: DeleteUserComponent
      },
      {
        path: 'users',
        component: UserTableComponent
      },
      {
        path: 'institutes',
        component: InstituteTableComponent
      },
      {
        path: 'add-institute',
        component: InstituteFormComponent
      },
      {
        path: '',
        redirectTo: 'users',
        pathMatch: 'full'
        // {
        //   path: '**',
        //   component: PageNotFoundComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
