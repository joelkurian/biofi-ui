export { ChangePasswordComponent } from './change-password/change-password.component';
export { DeleteUserComponent } from './delete-user/delete-user.component';
export { UserFormComponent } from './user-form/user-form.component';
export { UserTableComponent } from './user-table/user-table.component';
export { InstituteTableComponent } from './institute-table/institute-table.component';
export { InstituteFormComponent } from './institute-form/institute-form.component';
