import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';

import { ConfirmDeleteComponent } from '../confirm-delete/confirm-delete.component';
import { ApiService } from '../../core/api.service';

@Component({
  selector: 'bf-institute-table',
  templateUrl: './institute-table.component.html',
  styleUrls: ['./institute-table.component.css']
})
export class InstituteTableComponent implements OnInit {
  displayedColumns = ['id', 'name', 'email', 'address', 'actions'];
  dataSource;
  constructor(private api: ApiService, public dialog: MatDialog) {}

  ngOnInit() {
    this.api.getInstitutes().subscribe((institutes) => {
      this.dataSource = new MatTableDataSource(institutes);
    });
  }

  delete(institute) {
    console.log(institute);
    const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
      width: '250px',
      data: { objName: institute.name, obj: institute }
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
      console.log(result);
      if (result) {
        this.api.deleteInstitute(result.id).subscribe();
      }
    });
  }
}
