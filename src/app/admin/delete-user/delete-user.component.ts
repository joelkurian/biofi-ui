import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../core/api.service';

@Component({
  selector: 'bf-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss']
})
export class DeleteUserComponent implements OnInit {
  deleteUserForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private api: ApiService) {}

  ngOnInit() {
    this.deleteUserForm = this.formBuilder.group({
      userId: ['', Validators.required],
    });
  }

  onSubmit() {
    this.api.deleteUser(this.deleteUserForm.value.userId).subscribe((success) => console.log('s', success));
  }
}
