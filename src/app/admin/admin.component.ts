import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Component({
  selector: 'bf-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  user;

  constructor(private oauthService: OAuthService, private router: Router) {}

  ngOnInit() {
    Observable.fromPromise(this.oauthService.loadUserProfile()).subscribe((user) => (this.user = user));
  }

  logOut() {
    console.log('tt', this.oauthService.getIdToken());
    this.oauthService.logOut();
    this.router.navigate(['/']);
  }
}
