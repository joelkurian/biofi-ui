import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../core/api.service';

@Component({
  selector: 'bf-institute-form',
  templateUrl: './institute-form.component.html',
  styleUrls: ['./institute-form.component.css']
})
export class InstituteFormComponent implements OnInit {
  instituteForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private api: ApiService) {}

  ngOnInit() {
    this.instituteForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      address: ['', Validators.required]
    });
  }

  onSubmit() {
    this.api.addInstitute(this.instituteForm.value).subscribe((success) => console.log('s', success));
  }
}
