import { Component } from '@angular/core';
import { OAuthService, AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
  // issuer: 'https://steyer-identity-server.azurewebsites.net/identity',
  redirectUri: window.location.origin + '/index.html',
  // silentRefreshRedirectUri: window.location.origin + '/silent-refresh.html',
  clientId: 'biofi-ui',
  scope: 'all',
  dummyClientSecret: 'BiofiSecret',
  tokenEndpoint: '/oauth/v1/oauth/token',
  userinfoEndpoint: '/api/v1/user',
  requireHttps: false,
  oidc: false,
  logoutUrl: window.location.origin
};

@Component({
  selector: 'bf-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'bf';

  constructor(private oauthService: OAuthService) {
    this.configureWithNewConfigApi();
  }

  private configureWithNewConfigApi() {
    this.oauthService.configure(authConfig);
    // this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    // this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }
}
