import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { OAuthService } from 'angular-oauth2-oidc';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/fromPromise';

import { authConfig } from '../app.component';

@Injectable()
export class ApiService {
  user;

  constructor(private http: HttpClient, private oauthService: OAuthService) {}

  authenticate(credentials) {
    const headers = new HttpHeaders({
      Authorization: 'Basic ' + btoa(authConfig.clientId + ':' + authConfig.dummyClientSecret)
    });

    console.log(authConfig);
    return Observable.fromPromise(
      this.oauthService.fetchTokenUsingPasswordFlow(credentials.username, credentials.password, headers)
    );
  }

  logout() {
    this.oauthService.logOut();
  }

  addUser(user) {
    return this.http.post('/api/v1/users', new HttpParams({ fromObject: user }), {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });
  }

  deleteUser(userId) {
    return this.http.delete(`/api/v1/users/${userId}`);
  }

  getUsers() {
    return this.http.get<any>('/api/v1/users');
  }

  changePassword(passwords: { currentPassword: string; password: string }) {
    const params = new HttpParams()
      .set('currentPassword', passwords.currentPassword)
      .set('password', passwords.password);
    return this.http.put(`/api/users/${this.user.id}/password/reset`, params, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });
  }

  changePasswordByAdmin(userId, password) {
    return this.http.put(`/api/v1/users/${userId}/password`, new HttpParams().set('password', password));
  }

  getInstitutes() {
    return this.http.get<any>('/api/v1/institutes');
  }

  addInstitute(institute) {
    return this.http.post('/api/v1/institutes', new HttpParams({ fromObject: institute }), {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });
  }

  deleteInstitute(instituteId) {
    return this.http.delete(`/api/v1/institutes/${instituteId}`);
  }
}
